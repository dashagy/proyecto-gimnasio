class User {
  constructor(
    username,
    password,
    isAdmin = false,
    userId,
    membershipType = null
  ) {
    this.username = username;
    this.password = password;
    this.isCapped = false;
    this.isAdmin = isAdmin;
    this.userId = userId;
    this.isSuspended = false;
    this.signedin_classes = [];
    this.membershipType = membershipType;
    this.classes_per_week = this.membershipType + 2;
  }
}

class GymClass {
  constructor(name, schedule, cap) {
    this.name = name;
    this.schedule = schedule;
    this.cap = [];
    this.cap.push(cap);
    this.signed = this.initialize_clients_array();
  }

  initialize_clients_array() {
    let clients_array = [];
    for (let i = 0; i < this.schedule.length; i++) {
      clients_array.push(0);
    }
    return clients_array;
  }
}

function login_function(event) {
  event.preventDefault();
  let username = document.getElementById("loginUsername").value.toUpperCase();
  let password = document.getElementById("loginPassword").value;
  const [isValidUser, isAdmin] = login_validation_function(username, password);
  if (isValidUser) {
    if (isAdmin) {
      window.location.href = "./admin.html";
    } else {
      window.location.assign(
        `./common-signedin.html?signed_user=${username.toLowerCase()}`
      );
    }
  } else alert("Error al iniciar sesión");
}

function login_validation_function(usernameToValidate, passwordToValidate) {
  let users_array = user_database;
  users_array = users_array.filter(
    (user) => user.username == usernameToValidate
  );
  if (
    users_array.length != 0 &&
    passwordToValidate == users_array[0].password
  ) {
    if (users_array[0].firstLogIn) {
      let user_id = users_array[0].userId;
      let user = user_database.find(
        (registered_user) => registered_user.userId === user_id
      );
      change_password_function(user);
      localStorage.removeItem("user_database");
      localStorage.setItem("user_database", JSON.stringify(user_database));
    }
    return [true, users_array[0].isAdmin];
  } else return [false, false];
}

function get_user_info(username) {
  let signed_user = user_database.find(
    (user) => user.username === username.toUpperCase()
  );
  return signed_user;
}

function cap_user(user) {
  user.isCapped = user.signedin_classes.length >= user.classes_per_week;
}

function change_password_function(user) {
  user.password = prompt("Ingrese nueva contraseña");
  delete user.firstLogIn;
}

function create_user_function(event) {
  event.preventDefault();
  let username = document.getElementById("createUsername").value.toUpperCase();
  let password = document.getElementById("createPassword").value;
  let b_isAdmin = document.getElementById("isAdmin").checked;
  let membershipType = parseInt(
    document.getElementById("membershipType").value
  );
  if (verify_user_existence_function(username)) {
    let userId = create_user_id_function();
    let new_user = new User(
      username,
      password,
      b_isAdmin,
      userId,
      membershipType
    );
    if (!b_isAdmin) {
      new_user.firstLogIn = true;
    }
    user_database.push(new_user);
    localStorage.setItem("user_database", JSON.stringify(user_database));
    show_users(user_database);
  } else alert("Ya existe un usuario con ese nombre.");
}

function create_user_id_function() {
  let reference = user_database[user_database.length - 1];
  return reference == undefined ? 0 : reference.userId + 1;
}

function verify_user_existence_function(usernameToVerify) {
  let users_array = user_database.filter(
    (user) => user.username === usernameToVerify
  );
  return users_array.length == 0 ? true : false;
}

function suspend_user_function(id) {
  let aux = user_database.filter((user) => user.userId == id);
  if (aux[0].isSuspended) aux[0].isSuspended = 0;
  else aux[0].isSuspended = 1;
  localStorage.removeItem("user_database");
  localStorage.setItem("user_database", JSON.stringify(user_database));
  show_users(user_database);
}

function create_gym_class_function(event) {
  event.preventDefault();
  let current_date = new Date();
  let gym_class_name = document.getElementById("className").value.toUpperCase();
  let gym_class_day = document.getElementById("classScheduleDay").value;
  let gym_class_starting_time = document
    .getElementById("classStartingTime")
    .value.slice(0, -3);
  let existent_classes = main_schedule.filter(
    (element) =>
      element[0] == gym_class_day && element[1] == gym_class_starting_time
  );
  let class_already_exists = existent_class_verify(existent_classes.length);
  if (!class_already_exists) {
    gym_class_starting_time = parseInt(gym_class_starting_time);
    gym_class_finishing_time = gym_class_starting_time + 1;
    let gym_class_schedule = get_this_month_gym_classes(
      gym_class_starting_time,
      gym_class_finishing_time,
      gym_class_day,
      current_date.getDate(),
      current_date.getMonth(),
      current_date.getFullYear()
    );
    let index = gym_class_database.findIndex(
      (gym_classes) => gym_classes.name == gym_class_name
    );
    let gym_class_cap = parseInt(document.getElementById("classCap").value);
    if (index != -1) {
      gym_class_database[index].schedule = gym_class_database[
        index
      ].schedule.concat(gym_class_schedule);
      for (let i = 0; i < gym_class_schedule.length; i++) {
        gym_class_database[index].cap.push(gym_class_cap);
        gym_class_database[index].signed.push(0);
      }
      localStorage.removeItem("gym_class_database");
      localStorage.setItem(
        "gym_class_database",
        JSON.stringify(gym_class_database)
      );
    } else {
      new_class = new GymClass(
        gym_class_name,
        gym_class_schedule,
        gym_class_cap
      );
      gym_class_database.push(new_class);
      localStorage.setItem(
        "gym_class_database",
        JSON.stringify(gym_class_database)
      );
    }
    schedule_function(gym_class_database);
    admin_show_gym_classes(gym_class_database);
  } else alert("ERROR: Ya existe una clase en ese horario");
}

function schedule_function(classes) {
  let aux = [];
  for (let i = 0; i < classes.length; i++) {
    for (let j = 0; j < classes[i].schedule.length; j++) {
      let params = new Date(classes[i].schedule[j][0]);
      aux.push([params.getDay(), params.getHours() + 3]);
    }
  }
  localStorage.removeItem("main_schedule");
  localStorage.setItem("main_schedule", JSON.stringify(aux));
}

function existent_class_verify(num) {
  if (num > 0) return true;
  else return false;
}

/*function create_tables() {
  let node = document.createElement("TABLE");
  document
    .getElementById("adminPage")
    .appendChild(node)
    .setAttribute("id", "usersTable");
  node = document.createElement("TABLE");
  document
    .getElementById("adminPage")
    .appendChild(node)
    .setAttribute("id", "classesTable");
}*/

function show_users(users) {
  let membership_array = [
    "2 Clases/ Semana",
    "3 Clases/ Semana",
    "4 Clases/ Semana",
  ];
  let suspended_array = ["No", "Si"];
  let clients = users.filter((user) => user.isAdmin != true);
  let tableHtml = document.getElementById("usersTable");
  tableHtml.innerHTML = "";
  let user_table = `<tr>
      <th>ID</th>
      <th>Nombre</th>
      <th>Tipo de Supscripción</th>
      <th>Suspendido</th>
      <th>Suspender</th>
    </tr>`;
  clients.map((user) => {
    user_table += `<tr>
        <td>${user.userId}</td>
        <td>${user.username}</td>
        <td>${membership_array[user.membershipType]}</td>
        <td>${suspended_array[user.isSuspended]}</td>
        <td><button onclick="suspend_user_function(${
          user.userId
        })">Suspender</button></td>
      </tr>`;
  });
  tableHtml.innerHTML = user_table;
}

function admin_show_gym_classes(classes) {
  let days_array = [
    "Domingo",
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sábado",
  ];
  let months_array = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  let tableHtml = document.getElementById("classesTable");
  tableHtml.innerHTML = "";
  let class_table = `
    <tr>
      <th>Nombre</th>
      <th>Inicio</th>
      <th>Fin</th>
      <th>Capacidad</th>
      <th>Inscriptos</th>
    </tr>`;
  classes.map((gym_class) => {
    class_table += `<tr>
        <td rowspan="${gym_class.schedule.length}">${gym_class.name}</td>`;
    for (let i = 0; i < gym_class.schedule.length; i++) {
      let init_date = new Date(gym_class.schedule[i][0]);
      let end_date = new Date(gym_class.schedule[i][1]);
      let weekday = days_array[init_date.getDay()];
      let month = months_array[init_date.getMonth()];
      class_table += `
        <td>${weekday} ${init_date.getDate()} de ${month} a hs ${
        init_date.getHours() + 3
      }:00 </td>
        <td>${weekday} ${end_date.getDate()} de ${month} a hs ${
        end_date.getHours() + 3
      }:00 </td>
        <td>${gym_class.cap[i]}</td>
        <td>${gym_class.signed[i]}</td>
      </tr>`;
    }
  });
  tableHtml.innerHTML = class_table;
}

function show_gym_classes(classes) {
  let tableHtml = document.getElementById("classesSchedule");
  tableHtml.innerHTML = "";
  let table = `<colgroup>
                <col>
                <col style="background-color:#DCC48E;">
                <col style="background-color:#97DB9A;">
                <col style="background-color:#DCC48E;">
                <col style="background-color:#97DB9A;">
                <col style="background-color:#DCC48E;">
                <col style="background-color:#97DB9A;">
                <col style="background-color:#DCC48E;">
              </colgroup>
              <tr>
                <td>&nbsp;</td>
                <th>LUNES</th>
                <th>MARTES</th>
                <th>MIERCOLES</th>
                <th>JUEVES</th>
                <th>VIERNES</th>
                <th>SABADO</th>
                <th>DOMINGO</th>             
              </tr>`;
  for (let i = 8; i < 22; i++) {
    table += `<tr id="hour-${i}">
                <th>${i}:00</th>
                <td id="class-1-${i}"></td>
                <td id="class-2-${i}"></td>
                <td id="class-3-${i}"></td>
                <td id="class-4-${i}"></td>
                <td id="class-5-${i}"></td>
                <td id="class-6-${i}"></td>  
                <td id="class-0-${i}"></td>
            </tr>`;
  }
  tableHtml.innerHTML = table;
  let aux = classes;
  for (let i = 0; i < aux.length; i++) {
    let gym_class = aux[i];
    for (let j = 0; j < gym_class.schedule.length; j++) {
      let date = new Date(gym_class.schedule[j][0]);
      let weekday = date.getDay();
      let starting_hour = date.getHours() + 3;
      document.getElementById(`class-${weekday}-${starting_hour}`).innerHTML =
        gym_class.name;
    }
  }
}

function show_next_day_gym_classes(classes, is_suspended, user) {
  let is_signed = false;
  let is_user_capped = user.isCapped;
  let is_class_full = false;
  let days_array = [
    "Domingo",
    "Lunes",
    "Martes",
    "Miercoles",
    "Jueves",
    "Viernes",
    "Sábado",
  ];
  let months_array = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  let today = new Date();
  let day_index = (today.getDay() + 1) % 7;
  let day = today.getDate() + 1;
  let month = today.getMonth();
  let tableHtml = document.getElementById("classesSchedule");
  tableHtml.innerHTML = "";
  let table = `<colgroup>
                <col>
                <col style="background-color:#DCC48E;">
              </colgroup>
              <tr>
                <td>&nbsp;</td>
                <th>${days_array[day_index]}, ${day} de ${months_array[month]}</th>
                <th>Inscripción</th>
              </tr>`;
  for (let i = 8; i < 22; i++) {
    table += `<tr id="hour-${i}">
                <th>${i}:00</th>
                <td id="class-${days_array[day_index]}-${i}"></td>
                <td id="class-${days_array[day_index]}-${i}-sub"></td>
            </tr>`;
  }
  tableHtml.innerHTML = table;
  let aux = classes;
  for (let i = 0; i < aux.length; i++) {
    let gym_class = aux[i];
    for (let j = 0; j < gym_class.schedule.length; j++) {
      let date = new Date(gym_class.schedule[j][0]);
      let weekday = date.getDay();
      let starting_hour = date.getHours() + 3;
      is_signed = is_signed_in_gym_class(user, gym_class.name, starting_hour);
      if (weekday == day_index) {
        document.getElementById(
          `class-${days_array[day_index]}-${starting_hour}`
        ).innerHTML = gym_class.name;
        document.getElementById(
          `class-${days_array[day_index]}-${starting_hour}-sub`
        ).innerHTML = `<button id="${gym_class.name}-${starting_hour}" onclick="singin_gym_class('${gym_class.name}', ${starting_hour})">Inscribirse</button>
        <button id="${gym_class.name}-${starting_hour}-unsig" hidden onclick="unsingin_gym_class('${gym_class.name}', ${starting_hour})">Cancelar Inscripción</button>`;
        let sign_in_button = document.getElementById(
          `${gym_class.name}-${starting_hour}`
        );
        let cancel_button = document.getElementById(
          `${gym_class.name}-${starting_hour}-unsig`
        );
        is_class_full = gym_class.signed[j] >= gym_class.cap[j];
        if (is_suspended) {
          sign_in_button.setAttribute("disabled", "true");
          if (is_signed) {
            sign_in_button.setAttribute("hidden", "true");
            cancel_button.removeAttribute("hidden");
          }
        } else {
          if (is_signed) {
            sign_in_button.setAttribute("hidden", "true");
            cancel_button.removeAttribute("hidden");
          }
          if (is_user_capped) {
            sign_in_button.setAttribute("disabled", "true");
          } else {
            sign_in_button.removeAttribute("disabled");
          }
          if (is_class_full) {
            if (sign_in_button != null) {
              sign_in_button.setAttribute("disabled", "true");
            }
          }
        }
      }
    }
  }
}

function get_this_month_gym_classes(
  starting_hour,
  finishing_hour,
  scheduled_weekday,
  day,
  month,
  year
) {
  let date = new Date(year, month, day);
  let schedule = [];
  let days_array = [];
  while (date.getMonth() == month) {
    days_array.push(new Date(date));
    date.setDate(date.getDate() + 1);
  }
  days_array = days_array.filter((days) => days.getDay() == scheduled_weekday);
  for (let i = 0; i < days_array.length; i++) {
    let scheduled_year = days_array[i].getFullYear();
    let scheduled_month = days_array[i].getMonth();
    let scheduled_day = days_array[i].getDate();
    let date_starting = new Date(
      scheduled_year,
      scheduled_month,
      scheduled_day,
      starting_hour - 3
    );
    let date_finishing = new Date(
      scheduled_year,
      scheduled_month,
      scheduled_day,
      finishing_hour - 3
    );
    schedule[i] = [date_starting, date_finishing];
  }
  return schedule;
}

function singin_gym_class(gym_class_name, hour) {
  let url_string = window.location;
  let url = new URL(url_string);
  let signed_user_username = url.searchParams.get("signed_user");
  let user = user_database.filter(
    (client) => client.username === signed_user_username.toUpperCase()
  )[0];
  let is_signed = is_signed_in_gym_class(user, gym_class_name, hour);
  let today = new Date();
  let gym_class = gym_class_database.filter(
    (classes) => classes.name == gym_class_name
  )[0];
  let gym_class_index = gym_class_database.findIndex(
    (classes) => classes.name == gym_class_name
  );
  gym_class_schedule_index = gym_class.schedule.findIndex(
    (start_time) =>
      new Date(start_time[0]).getDate() == today.getDate() + 1 &&
      new Date(start_time[0]).getHours() + 3 == hour
  );
  let cap_condition =
    gym_class.signed[gym_class_schedule_index] <
    gym_class.cap[gym_class_schedule_index]
      ? true
      : false;
  if (cap_condition) {
    if (
      confirm(
        `¿Seguro que desea inscribirse en la clase de ${gym_class_name} de mañana a hs. ${hour}:00?`
      )
    ) {
      user.signedin_classes.push([
        gym_class_name,
        gym_class.schedule[gym_class_schedule_index],
      ]);
      cap_user(user);
      gym_class_database[gym_class_index].signed[gym_class_schedule_index]++;
      localStorage.removeItem("user_database");
      localStorage.setItem("user_database", JSON.stringify(user_database));
      localStorage.removeItem("gym_class_database");
      localStorage.setItem(
        "gym_class_database",
        JSON.stringify(gym_class_database)
      );
    }
  }
  common_signedin_function();
}

function unsingin_gym_class(gym_class_name, hour) {
  let url_string = window.location;
  let url = new URL(url_string);
  let signed_user_username = url.searchParams.get("signed_user");
  let user = user_database.filter(
    (client) => client.username === signed_user_username.toUpperCase()
  )[0];
  let today = new Date();
  let gym_class = gym_class_database.filter(
    (classes) => classes.name == gym_class_name
  )[0];
  let gym_class_index = gym_class_database.findIndex(
    (classes) => classes.name == gym_class_name
  );
  gym_class_schedule_index = gym_class.schedule.findIndex(
    (start_time) =>
      new Date(start_time[0]).getDate() == today.getDate() + 1 &&
      new Date(start_time[0]).getHours() + 3 == hour
  );
  if (
    confirm(
      `¿Seguro que desea darse de baja en la clase de ${gym_class_name} de mañana a hs. ${hour}:00?`
    )
  ) {
    let index = user.signedin_classes.findIndex(
      (classes) =>
        classes[0] == gym_class_name &&
        new Date(classes[1][0]).getHours() + 3 == hour
    );
    user.signedin_classes.splice(index, 1);
    gym_class_database[gym_class_index].signed[gym_class_schedule_index]--;
    localStorage.removeItem("user_database");
    localStorage.setItem("user_database", JSON.stringify(user_database));
    localStorage.removeItem("gym_class_database");
    localStorage.setItem(
      "gym_class_database",
      JSON.stringify(gym_class_database)
    );
  }
  cap_user(user);
  common_signedin_function();
}

function is_signed_in_gym_class(user, gym_class_name, hour) {
  let aux = user.signedin_classes.filter(
    (classes) =>
      classes[0] == gym_class_name &&
      new Date(classes[1][0]).getHours() + 3 == hour
  );
  if (aux.length > 0) return true;
  else return false;
}

function common_signedin_function() {
  let is_suspended = false;
  let signed_user = "";
  let url_string = window.location;
  let url = new URL(url_string);
  let signed_user_username = url.searchParams.get("signed_user");

  if (signed_user_username != null) {
    signed_user = get_user_info(signed_user_username);
    if (signed_user.isSuspended == true) {
      alert("El usuario está suspendido");
      is_suspended = true;
    }
    document.getElementById("username").innerHTML = signed_user.username;
    show_next_day_gym_classes(gym_class_database, is_suspended, signed_user);
  }
}

let user_database = JSON.parse(localStorage.getItem("user_database")) || [];
let gym_class_database =
  JSON.parse(localStorage.getItem("gym_class_database")) || [];
let main_schedule = JSON.parse(localStorage.getItem("main_schedule")) || [];

function MAIN() {
  let url_string = window.location;
  let url = new URL(url_string);
  let subpage = url.pathname;
  if (subpage.includes("horarios")) show_gym_classes(gym_class_database);
  common_signedin_function();
  show_users(user_database);
  admin_show_gym_classes(gym_class_database);
}

MAIN();
